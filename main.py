import discord
from config import TOKEN
from config import PREFIX
import jsonshit
client = discord.Client()

print('Logging into discord...')

@client.event
async def on_ready():
    print('logged in as {0.user}'.format(client))


@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if message.content.startswith(PREFIX + 'help'):
        await message.channel.send('I\'m quite literally only here to post pxl testimonials. Try pxl.testimonial')
    if message.content.startswith(PREFIX + 'hello'):
        await message.channel.send('Hello!')
    if message.content.startswith(PREFIX + 'blackwinneryoshi'):
        await message.channel.send('you weren\'t expecting this command were you')
    if message.content.startswith(PREFIX + 'github'):
        await message.channel.send('https://gitlab.com/MazariFox/pxl-testimonial-discordbot enjoy')
    if message.content.startswith(PREFIX +'testimonial'):
            tst = jsonshit.get_tst()

            if tst == False:
                await message.channel.send("Yell at relative to fix it. The API is down.")
            else:
                await message.channel.send('``' + tst['testimonial']['testimonial'] + '\n' + '--' + tst['testimonial']['author'] + '``')

client.run(TOKEN)