import requests
URL = 'https://api.pxl.blue/testimonial'
def check_valid_status_code(request):
    if request.status_code == 200:
        return request.json()

    return False
def get_tst():
    request = requests.get(URL)
    data = check_valid_status_code(request)

    return data